﻿using Autofac;
using MahApps.Metro.Controls;
using ODBmvvm1.Model;
using ODBmvvm1.Model.Interfaces;
using ODBmvvm1.ViewModel;
using System.Windows;
using System.Windows.Interactivity;

namespace ODBmvvm1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<Address>().As<IAddress>();
            builder.RegisterType<Contact>().As<IContact>();
            builder.RegisterType<DbHelper>().As<IDbHelper>();
            builder.RegisterType<Phone>().As<IPhoneInfo>();
            builder.RegisterType<ContactInfo>().As<IContactInfo>();
            builder.RegisterType<InfoWindowViewModel>().As<IInfoWindowViewModel>();
            builder.RegisterType<MainViewModel>().As<IMainViewModel>();
            builder.RegisterType<EditContactViewModel>().As<IEditContactViewModel>();



            //todo inject editwindow in to a constructor mvm
            var container = builder.Build();
            var viewModel = container.Resolve<IMainViewModel>();
            this.DataContext = viewModel;
            InitializeComponent();
        }
    }
}
