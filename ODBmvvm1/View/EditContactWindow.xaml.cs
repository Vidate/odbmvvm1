﻿using MahApps.Metro.Controls;
using ODBmvvm1.Model;
using ODBmvvm1.Model.Interfaces;
using ODBmvvm1.ViewModel;

namespace ODBmvvm1.View
{
    /// <summary>
    /// Interaction logic for EditContactWindow.xaml
    /// </summary>
    public partial class EditContactWindow : MetroWindow
    {
        public EditContactWindow(IDbHelper dbHelper,Contact concact)
        {
            this.DataContext = new EditContactViewModel(dbHelper,concact);
            InitializeComponent();
        }
    }
}
