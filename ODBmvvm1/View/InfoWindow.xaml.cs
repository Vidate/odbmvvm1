﻿using MahApps.Metro.Controls;
using ODBmvvm1.Model;
using ODBmvvm1.Model.Interfaces;
using ODBmvvm1.ViewModel;

namespace ODBmvvm1.View
{
    /// <summary>
    /// Interaction logic for InfoWindow.xaml
    /// </summary>
    public partial class InfoWindow : MetroWindow
    {
        public InfoWindow(IContactInfo contactInfo)
        {
            this.DataContext = new InfoWindowViewModel(contactInfo);
            InitializeComponent();
        }

        private void ColoseInfoWindow_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
