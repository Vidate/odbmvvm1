﻿using Db4objects.Db4o;
using ODBmvvm1.Converters;
using ODBmvvm1.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBmvvm1.Model
{
    class DbHelper : IDbHelper
    {
        private IObjectContainer _dbContext;
        private string _dataBaseFileName;
        public DbHelper()
        {

        }
        ~DbHelper()
        {
            try
            {
                DbContext.Close();
                DbContext.Dispose();
            }
            catch (Exception) { throw; }
        }
        public ICollection<Contact> GetContacts
        {
            get
            {
                try
                {
                    IObjectSet result = DbContext.QueryByExample(new Contact(null, null, null, null));
                    return result.OfType<Contact>().ToList();
                }
                catch (Exception) { throw new NotImplementedException(); }
            }
        }

        public string DataBaseFileName
        {
            get { return _dataBaseFileName; }
            set
            {
                _dataBaseFileName = value;
                try
                {
                    DbContext.Close();
                    DbContext.Dispose();
                }
                catch (Exception)
                { }
                finally
                {
                    DbContext = Db4oEmbedded.OpenFile(DataBaseFileName);
                }
            }
        }
        public IObjectContainer DbContext
        {
            get { return _dbContext; }
            set { _dbContext = value; }
        }

        public ContactInfo GetContactInfo
        {
            get
            {
                var contactNumber = DbContext.Query<Contact>().Count();
                var phoneInfoNumber = DbContext.Query<Phone>().Count();
                var addressNumber = DbContext.Query<Address>().Count();
                return new ContactInfo(phoneInfoNumber.ToString(), addressNumber.ToString(), contactNumber.ToString());
            }
        }

        //todo 
        public void EditAddress(Contact contactEdited)
        {
            IObjectSet result = DbContext.QueryByExample(new Contact(contactEdited.Name, contactEdited.Surname));
            var contact = (Contact)result.Next();
            contact.Address.City = contactEdited.Address.City;
            contact.Address.PostCode = contactEdited.Address.PostCode;
            contact.Address.Street = contactEdited.Address.Street;
            DbContext.Store(contact.Address);
            DbContext.Commit();
        }

        public void AddContact(Contact contact)
        {
            DbContext.Store(contact);
            DbContext.Commit();
        }
        public void AddPhone(Contact contact, Phone phoneInfo)
        {
            IObjectSet result = DbContext.QueryByExample(new Contact(
                contact.Name,
                contact.Surname));
            var newContact = (Contact)result.OfType<Contact>().FirstOrDefault();
            newContact.PhonesNumbers.Add(new Phone(phoneInfo.Number,
                phoneInfo.NetworkOperator,
                phoneInfo.Type));
            DbContext.Store(newContact.PhonesNumbers);
            DbContext.Commit();
        }

        public ICollection<Contact> ContactsFromPath(string path)
        {
            throw new NotImplementedException();
        }
        public void EditContact(Contact contactOriginal, Contact contact)
        {
            IObjectSet result = DbContext.QueryByExample(new Contact(contactOriginal.Name, contactOriginal.Surname));
            var tmp = result.OfType<Contact>().FirstOrDefault();
            tmp.Name = contact.Name;
            tmp.Surname = contact.Surname;
            DbContext.Store(tmp);
            DbContext.Commit();
        }

        public void EditPhone(Contact contact, Phone phoneInfoOriginal, Phone phoneInfoEdited)
        {
            IObjectSet result = DbContext.QueryByExample(new Phone(
                phoneInfoOriginal.Number,
                phoneInfoOriginal.NetworkOperator,
                phoneInfoOriginal.Type));
            var phoneInfo = result.OfType<Phone>().FirstOrDefault();
            phoneInfo.Number = phoneInfoEdited.Number;
            phoneInfo.NetworkOperator = phoneInfoEdited.NetworkOperator;
            phoneInfo.Type = phoneInfoEdited.Type;
            DbContext.Store(phoneInfo);
            DbContext.Commit();
        }

        public void RemoveAddress(Contact contact)
        {
            IObjectSet result = DbContext.QueryByExample(new Contact(contact.Name, contact.Surname));
            var contactOrginal = result.OfType<Contact>().FirstOrDefault();
            contactOrginal.Address.City = "";
            contactOrginal.Address.Street = "";
            contactOrginal.Address.PostCode = "";
            DbContext.Store(contactOrginal.Address);
            DbContext.Commit();
        }

        public void RemoveContact(Contact contact)
        {
            foreach (var item in contact.PhonesNumbers)
            {
                try
                {
                    DbContext.Delete(item);
                }
                catch (Exception) { }
            }
            DbContext.Delete(contact.Address);
            DbContext.Delete(contact);
            DbContext.Commit();
        }

        public void RemovePhone(Contact contact, Phone phoneInfo)
        {
            IObjectSet result = DbContext.QueryByExample(new Contact(contact.name, contact.Surname));
            var contactOrginal = result.OfType<Contact>().FirstOrDefault();
            var phoneInfoTmp = contactOrginal.PhonesNumbers.Where(x =>
            x.Number == phoneInfo.Number &&
            x.NetworkOperator == phoneInfo.NetworkOperator &&
            x.Type == phoneInfo.Type).FirstOrDefault();

            var toRemove = contactOrginal.PhonesNumbers.Where(x =>
             x.Number == phoneInfo.Number &&
             x.NetworkOperator == phoneInfo.NetworkOperator &&
             x.Type == phoneInfo.Type).FirstOrDefault();
            contactOrginal.PhonesNumbers.Remove(toRemove);
            DbContext.Store(contactOrginal.PhonesNumbers);
            DbContext.Delete(phoneInfoTmp);
            DbContext.Commit();
        }
    }
}
