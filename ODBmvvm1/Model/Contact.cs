﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBmvvm1.Model
{
    public class Contact : IContact
    {
        public string name;
        public string surname;
        public Address address;
        public List<Phone> phonesNumbers;
        public string Name { get { return name; } set { name = value; } }
        public Address Address { get { return address; } set { address = value; } }
        public string Surname { get { return surname; } set { surname = value; } }
        public List<Phone> PhonesNumbers { get { return phonesNumbers; } set { phonesNumbers = value; } }
       
        public Contact(string name, string surname, Address adres, List<Phone> phonesNumbers)
        {
            this.Name = name;
            this.Surname = surname;
            this.Address = adres;
            this.PhonesNumbers = phonesNumbers;
        }
        public Contact(string name, string surname)
        {
            this.Name = name;
            this.Surname = surname;
            this.Address = new Address();
            this.PhonesNumbers = new List<Phone>();
        }
        public string Info()
        {
            string phoneInfo = "";
            foreach (var item in this.phonesNumbers)
            {
                phoneInfo += item.Number + " " + item.NetworkOperator + " " + item.Type + "\r\n";
            }
            return "Imie" + this.name
                + "\r\nNazwisko:" + this.surname
                + "\r\nMiasto: " + this.address.City
                + "\r\nUlica: " + this.address.Street
                + "\r\nKod potczowy:" + this.address.PostCode
                + phoneInfo;
        }
    }
}
