﻿using ODBmvvm1.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBmvvm1.Model
{
    public class ContactInfo : IContactInfo
    {
        private string _numberOfContacts;
        private string _numberOfAddress;
        private string _numberOfPhones;
        public string NumberOfContacts
        {
            get { return _numberOfContacts; }
            set { _numberOfContacts = value; }
        }
        public string NumberOfAddress
        {
            get { return _numberOfAddress; }
            set { _numberOfAddress = value; }
        }
        public string NumberOfPhones
        {
            get { return _numberOfPhones; }
            set { _numberOfPhones = value; }
        }
        public ContactInfo(string numberOfPhones, string numberOfAddress, string numberOfContacts)
        {
            this.NumberOfPhones = numberOfPhones;
            this.NumberOfAddress = numberOfAddress;
            this.NumberOfContacts = numberOfContacts;
        }
    }
}
