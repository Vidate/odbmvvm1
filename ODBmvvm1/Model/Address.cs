﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBmvvm1.Model
{
    public class Address : IAddress
    {
        private string _street;
        private string _city;
        private string _postCode;
        public string Street { get { return _street; } set { _street = value; } }
        public string City { get { return _city; } set { _city = value; } }
        public string PostCode { get { return _postCode; } set { _postCode = value; } }
        public Address(string street, string postCode, string city)
        {
            this.Street = street;
            this.PostCode = postCode;
            this.City = city;
        }
        public Address()
        {
        }
    }
}
