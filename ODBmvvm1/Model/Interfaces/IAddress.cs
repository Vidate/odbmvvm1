﻿namespace ODBmvvm1.Model
{
    public interface IAddress
    {
        string City { get; set; }
        string PostCode { get; set; }
        string Street { get; set; }
    }
}