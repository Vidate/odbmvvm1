﻿using System.Collections.Generic;

namespace ODBmvvm1.Model
{
    public interface IContact
    {
        Address Address { get; set; }
        string Name { get; set; }
        List<Phone> PhonesNumbers { get; set; }
        string Surname { get; set; }

        string Info();
    }
}