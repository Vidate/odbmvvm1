﻿using Db4objects.Db4o;
using ODBmvvm1.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBmvvm1.Model.Interfaces
{
    public interface IDbHelper
    {
        IObjectContainer DbContext { get; set; }
        ICollection<Contact> ContactsFromPath(string path);
        ICollection<Contact> GetContacts { get; }
        ContactInfo GetContactInfo { get; }
        string DataBaseFileName { get; set; }
        //Concats
        void AddContact(Contact contact);
        void EditContact(Contact contactOriginal, Contact contact);
        void RemoveContact(Contact contact);
        //Address
        void EditAddress(Contact contact);
        void RemoveAddress(Contact contact);
        //Phones
        void AddPhone(Contact contact, Phone phoneInfo);
        void EditPhone(Contact contact,Phone phoneInfoOriginal,Phone phoneInfoEdited);
        void RemovePhone(Contact contact,Phone phoneInfo);

    }
}
