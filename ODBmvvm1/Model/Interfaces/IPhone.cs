﻿namespace ODBmvvm1.Model
{
    public interface IPhoneInfo
    {
        string NetworkOperator { get; set; }
        string Number { get; set; }
        string Type { get; set; }
    }
}