﻿namespace ODBmvvm1.Model
{
    public interface IContactInfo
    {
        string NumberOfAddress { get; set; }
        string NumberOfContacts { get; set; }
        string NumberOfPhones { get; set; }
    }
}