﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBmvvm1.Model
{
    public class Phone : IPhoneInfo
    {
        private string _number;
        private string _networkOperator;
        private string _type;
        public string Number { get { return _number; } set { _number = value; } }
        public string NetworkOperator { get { return _networkOperator; } set { _networkOperator = value; } }
        public string Type { get { return _type; } set { _type = value; } }

        public Phone(string number, string networkOperator, string type)
        {
            this.Number = number;
            this.NetworkOperator = networkOperator;
            this.Type = type;
        }
    }
}
