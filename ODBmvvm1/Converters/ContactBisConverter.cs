﻿using ODBmvvm1.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ODBmvvm1.Converters
{
    public class ContainerPhone
    {
        public Contact orginal;
        public Phone orginalPhoneInfo;
        public string Number;
        public string Operator;
        public string Typ;
        public ContainerPhone(Contact orginal, Phone orginalPhoneInfo, string Number, string Operator, string Typ)
        {
            this.orginal = orginal;
            this.Number = Number;
            this.Operator = Operator;
            this.Typ = Typ;
            this.orginalPhoneInfo= orginalPhoneInfo;
        }
    }
    public class ContainerContact
    {
        public Contact orginal;
        public string name;
        public string surname;

        public ContainerContact(Contact orginal, string name, string surname)
        {
            this.orginal = orginal;
            this.name = name;
            this.surname = surname;
        }
    }

    public class ContactBisConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            return new ContainerContact((Contact)values[0], (string)values[1], (string)values[1]);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
