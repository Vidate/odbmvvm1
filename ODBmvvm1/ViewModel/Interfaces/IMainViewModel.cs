﻿using System.Collections.ObjectModel;
using ODBmvvm1.Model;

namespace ODBmvvm1.ViewModel
{
    public interface IMainViewModel
    {
        ObservableCollection<Contact> Contacts { get; set; }
        Contact ContactToEdit { get; set; }
        Contact SelectedContact { get; set; }
        Phone SelectedPhone { get; set; }

        void addContactCommand(Contact contact);
        void editContact();
        void openEditWindowCommand();
        void removeContact();
        //void removePhoneCommand(PhoneInfo contact);
        void selectItemCommand(Contact contact);
    }
}