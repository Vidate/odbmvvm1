﻿using ODBmvvm1.Model;

namespace ODBmvvm1.ViewModel
{
    public interface IEditContactViewModel
    {
        Contact CurrentContact { get; set; }
    }
}