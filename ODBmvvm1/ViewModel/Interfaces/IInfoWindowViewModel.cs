﻿using ODBmvvm1.Model;

namespace ODBmvvm1.ViewModel
{
    public interface IInfoWindowViewModel
    {
        IContactInfo ContactInfo { get; set; }
    }
}