﻿using Commander;
using MahApps.Metro.Controls;
using ODBmvvm1.Converters;
using ODBmvvm1.Model;
using ODBmvvm1.Model.Interfaces;
using ODBmvvm1.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBmvvm1.ViewModel
{
    public class MainViewModel : IMainViewModel
    {
        private Contact _contactToEdit;
        private ObservableCollection<Contact> _contacts;
        private Contact _selectedContact;
        private Phone _selectedPhone;

        public MainViewModel(IDbHelper dbHelper)
        {
            this._dbHelper = dbHelper;
            _dbHelper.DataBaseFileName = "test2.dsr3d";
            Contacts = ObservableCollectionConverter.ToObservableCollection<Contact>(_dbHelper.GetContacts);
        }
        public Phone SelectedPhone
        {
            get { return _selectedPhone; }
            set { _selectedPhone = value; }
        }

        private readonly IDbHelper _dbHelper;

        public Contact ContactToEdit
        {
            get { return _contactToEdit; }
            set { _contactToEdit = value; }
        }
        public Contact SelectedContact
        {
            get { return _selectedContact; }
            set { _selectedContact = value; }
        }
        public ObservableCollection<Contact> Contacts
        {
            get { return _contacts; }
            set { _contacts = value; }
        }
        [OnCommand("OpenInfoWindowCommand")]
        public void OpenInfoWindowCommand()
        {
            Contacts = ObservableCollectionConverter.ToObservableCollection<Contact>(_dbHelper.GetContacts);
            InfoWindow infoWindow = new InfoWindow(_dbHelper.GetContactInfo);
            infoWindow.Show();
        }
        [OnCommand("OpenEditWindowCommand")]
        public void openEditWindowCommand()
        {
            EditContactWindow editContactWindow = new EditContactWindow(this._dbHelper, SelectedContact);
            editContactWindow.Show();
        }
        [OnCommand("AddContactCommand")]
        public void addContactCommand(Contact contact)
        {
            Contacts.Add(contact);
            _dbHelper.AddContact(contact);
            Contacts = ObservableCollectionConverter.ToObservableCollection<Contact>(_dbHelper.GetContacts);
        }
        [OnCommand("RemoveContactCommand")]
        public void removeContact()
        {
            _dbHelper.RemoveContact(SelectedContact);
            //Contacts.Remove(SelectedContact);
            Contacts = ObservableCollectionConverter.ToObservableCollection<Contact>(_dbHelper.GetContacts);
        }

        [OnCommand("SelectItemCommand")]
        public void selectItemCommand(Contact contact)
        {
            SelectedContact = contact;
        }
        [OnCommand("EditContactCommand")]
        public void editContact()
        {
            EditContactWindow editContactWindow = new EditContactWindow(_dbHelper, SelectedContact);
            editContactWindow.Show();
        }
    }
}
