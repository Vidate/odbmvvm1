﻿using Commander;
using ODBmvvm1.Converters;
using ODBmvvm1.Model;
using ODBmvvm1.Model.Interfaces;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBmvvm1.ViewModel
{
    [ImplementPropertyChanged]
    public class EditContactViewModel : IEditContactViewModel
    {
        private Contact _currentContact;
        private Phone _selectedNumber;
        private readonly IDbHelper _dbHelper;
        private bool _enableEdit;
        private ObservableCollection<Phone> _phoneList;

        public EditContactViewModel(IDbHelper dbHelper, Contact contact)
        {
            this._dbHelper = dbHelper;
            var con = _dbHelper.GetContacts.Where(x => x.Name == contact.Name && x.Surname == contact.Surname).FirstOrDefault();
            this.CurrentContact = con;
            this.PhoneList = ObservableCollectionConverter.ToObservableCollection(con.PhonesNumbers);
        }

        [OnCommand("EditAddressInfoCommand")]
        public void editAddressInfoCommand(Address address)
        {
            CurrentContact.Address.City = address.City;
            CurrentContact.Address.PostCode = address.PostCode;
            CurrentContact.Address.Street = address.Street;
            _dbHelper.EditAddress(CurrentContact);
        }
        [OnCommand("EditContactInfoCommand")]
        public void editContactInfoCommand(Contact contact)
        {
            _dbHelper.EditContact(CurrentContact, contact);
        }
        [OnCommand("RemovePhoneNummberCommand")]
        public void removePhoneNummberCommand(Phone phoneInfo)
        {
            _dbHelper.RemovePhone(CurrentContact, phoneInfo);
            PhoneList.Remove(phoneInfo);
        }
        [OnCommand("AddNewPhoneNumberCommand")]
        public void addNewPhoneNumberCommand(Phone phoneInfo)
        {
            _dbHelper.AddPhone(CurrentContact, phoneInfo);
            PhoneList.Add(phoneInfo);
        }
        [OnCommand("EditPhoneNumberCommand")]
        public void editPhoneNumberCommand(Phone phoneInfo)
        {
            _dbHelper.EditPhone(CurrentContact, SelectedNumber, phoneInfo);
            PhoneList = ObservableCollectionConverter.ToObservableCollection(CurrentContact.PhonesNumbers);

        }
        public ObservableCollection<Phone> PhoneList
        {
            get { return _phoneList; }
            set { _phoneList = value; }
        }

        public bool EnableEdit
        {
            get { return _enableEdit; }
            set { _enableEdit = value; }
        }

        public Contact CurrentContact
        {
            get { return _currentContact; }
            set { _currentContact = value; }
        }

        [OnCommand("SelectNumberCommand")]
        public void selectItemCommand(Phone phoneInfo)
        {
            SelectedNumber = phoneInfo;
        }
        public Phone SelectedNumber
        {
            get { return _selectedNumber; }
            set { _selectedNumber = value; }
        }

    }
}
