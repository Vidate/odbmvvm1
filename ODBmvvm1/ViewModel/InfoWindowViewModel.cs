﻿using ODBmvvm1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODBmvvm1.ViewModel
{
    public class InfoWindowViewModel : IInfoWindowViewModel
    {
        private IContactInfo _contactInfo;
        public IContactInfo ContactInfo
        {
            get { return _contactInfo; }
            set { _contactInfo = value; }
        }
        public InfoWindowViewModel(IContactInfo contactInfo)
        {
            this.ContactInfo = contactInfo;
        }
    }
}
